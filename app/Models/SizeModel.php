<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SizeModel extends Model
{
    protected $table = 'sizes';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'code', 'status'];
    use HasFactory;
}
