<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ColorsModel extends Model
{
    protected $table = 'colors';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'code', 'status'];
    use HasFactory;
}
