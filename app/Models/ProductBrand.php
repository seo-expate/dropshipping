<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductBrand extends Model
{
    protected $table = 'product_brands';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'status', 'image', 'short_description', 'meta_tag', 'meta_description'];
    use HasFactory;
}
