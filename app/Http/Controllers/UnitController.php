<?php

namespace App\Http\Controllers;

use App\Models\UnitModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UnitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = array(
            'page_name' => 'Unit',
            'arr' => UnitModel::all(),
        );
        return view('unit.index')->with('data' , $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = array(
            'page_name' => 'Add Unit',
        );
        return view('unit.create')->with('data' , $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();
        request()->validate([
            'name' => 'required|max:20|min:1|regex:/^[A-Za-z_-]/',           
            'code' => 'required|max:20|min:1|regex:/^[A-Za-z]/',
        ]);

        
        if(isset($input['image'])){            
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('image/unit', $file_name, 'public');
            $input['image'] = $file_name;
        }else{
            $input['image'] = '';
        }

        unset($input['_token']);
        UnitModel::create($input);        
        return redirect('unit')->with('flash_message' , "Unit Added");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = array(
            'page_name' => 'Unit',
            'arr' => UnitModel::find($id),
        );

        return view('unit.show')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $category = UnitModel::find($id);
        $input = $request->all();
        

        request()->validate([
            'name' => 'required|max:20|min:1|regex:/^[A-Za-z_-]/',            
            'code' => 'required|max:20|min:1|regex:/^[0-9]+$/',            
        ]);

        $category->update($input);
        return redirect('unit')->with('flash_message', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $unit = UnitModel::find($id);        
        UnitModel::destroy($id);
        return redirect('unit')->with('del_message', 'Deleted!');
    }
}
