<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Models\SubCategory;
use Illuminate\Support\Facades\Storage;


class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = array(
            'page_name' => 'Sub Category',
            'arr' => SubCategory::all(),
        );
        return view('sub_category.index')->with('data' , $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = array(
            'page_name' => 'Create New Sub Category',
            'arr' => Category::all(),
        );
        
        return view('sub_category.create')->with('data' , $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();        
        request()->validate([
            'name' => 'required|max:20|min:3|regex:/^[A-Za-z_-]/',   
        ]);
        
        if(isset($input['image'])){            
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('image/sub_category', $file_name, 'public');
            $input['image'] = $file_name;
        }else{
            $input['image'] = '';
        }

        unset($input['_token']);
        SubCategory::create($input);        
        return redirect('sub_category')->with('flash_message' , "Sub Category Added");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = array(
            'page_name' => 'Sub Category',
            'arr' => SubCategory::find($id),
            'arr_cat' => Category::all(),
        );

        //echo '<pre>';print_r($data['arr']);echo '</pre>';die;

        return view('sub_category.show')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $category = SubCategory::find($id);
        $input = $request->all();
        
        if(isset($input['image'])){            
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('image/sub_category', $file_name, 'public');
            $input['image'] = $file_name;
            Storage::disk('public')->delete('image/sub_category/' . $input['old_img']);
        }

        

        request()->validate([
            'name' => 'required|max:20|min:3|regex:/^[A-Za-z_-]/',            
        ]);

        unset($input['password']);

        $category->update($input);
        return redirect('sub_category')->with('flash_message', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $SubCategory = SubCategory::find($id);
        Storage::disk('public')->delete('image/sub_category/' . $SubCategory['image']);
        SubCategory::destroy($id);
        return redirect('sub_category')->with('del_message', 'Deleted!');
    }

    public function get_sub_category(Request $request){

        if($request->ajax()){
            $id =  $_GET['category_id'];
            $arr = SubCategory::where('category_id', $id)->get();
            return view('sub_category.get_sub_category', compact('arr'));
        }
        return redirect('dashboard');
    }
}
