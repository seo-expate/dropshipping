<?php

namespace App\Http\Controllers;

use App\Models\SizeModel;
use Illuminate\Http\Request;

class SizeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = array(
            'page_name' => 'Size',
            'arr' => SizeModel::all(),
        );
        return view('size.index')->with('data' , $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = array(
            'page_name' => 'Add Size',
        );
        return view('size.create')->with('data' , $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();
        request()->validate([
            'name' => 'required|max:20|min:1|regex:/^[A-Za-z_-]/',            
            'code' => 'required|max:20|min:1',
        ]);
        

        unset($input['_token']);
        SizeModel::create($input);        
        return redirect('size')->with('flash_message' , "Size Added");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = array(
            'page_name' => 'Colors',
            'arr' => SizeModel::find($id),
        );

        return view('size.show')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $category = SizeModel::find($id);
        $input = $request->all();
        

        request()->validate([
            'name' => 'required|max:20|min:1|regex:/^[A-Za-z_-]/',            
            'code' => 'required|max:20|min:1',           
        ]);

        $category->update($input);
        return redirect('size')->with('flash_message', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
