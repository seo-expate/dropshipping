<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Customer;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = array(
            'page_name' => 'Customer',
            'customers' => Customer::all(),
        );
        return view('customer.index')->with('data' , $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = array(
            'page_name' => 'Create New Customer',
        );
        return view('customer.create')->with('data' , $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {        
        $input = $request->all();
        request()->validate([
            'name' => 'required|max:20|min:3|regex:/^[A-Za-z_-]/',
            'email' => 'required|email|min:5|max:100|unique:customer',            
            'phone' => 'required|max:50',            
            'password' => 'required|min:5|max:50',
            'address' => 'required|min:5|max:50',
        ]);

        $input['password'] = Hash::make($input['password']);
        
        if(isset($input['image'])){            
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('image', $file_name, 'public');
            $input['image'] = $file_name;
        }else{
            $input['image'] = '';
        }

        unset($input['_token']);
        Customer::create($input);        
        return redirect('customer')->with('flash_message' , "Customer Added");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = array(
            'page_name' => 'Customer Profile',
            'customers' => Customer::find($id),
        );
        

        return view('customer.show')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        echo "edit";die;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {        
        $customer = Customer::find($id);
        $input = $request->all();


        if (array_key_exists("current_password",$input)){
            
            $user = Customer::find($input['id']);            

            # Validation
            $request->validate([
                'current_password' => 'required',
                'new_password' => 'required|confirmed|',
                'new_password_confirmation' => 'required|same:new_password|different:current_password',
            ]);

            #Match The Old Password
            if(Hash::check($input['current_password'], $user['password'])){
                
                $new_password['password'] = Hash::make($input['new_password']);
                $customer->update($new_password);
                return redirect('customer')->with('flash_message', 'Password has been changed');
            }else{
                return redirect('customer')->with('flash_message', 'Password current password not correct');
            }

        }


        if(isset($input['image'])){            
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('image', $file_name, 'public');
            $input['image'] = $file_name;
            Storage::disk('public')->delete('image/category' . $input['old_img']);
        }

        //echo '<pre>';print_r($input);echo '</pre>';die;

        request()->validate([
            'name' => 'required|max:20|min:3|regex:/^[A-Za-z_-]/',
            'email' => Rule::unique('customer')->ignore($id),
            'phone' => 'required|max:50',
            'address' => 'required|min:5|max:50',
            'status' => 'required',
        ]);

        unset($input['password']);

        $customer->update($input);
        return redirect('customer')->with('flash_message', 'Updated');
        
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $customer = Customer::find($id);
        Storage::disk('public')->delete('image/' . $customer['image']);
        Customer::destroy($id);
        return redirect('customer')->with('del_message', 'Deleted!');
    }
}
