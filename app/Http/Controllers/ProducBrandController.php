<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\ProductBrand;
use Illuminate\Support\Facades\Storage;


class ProducBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = array(
            'page_name' => 'Product Brand',
            'arr' => ProductBrand::all(),
        );
        return view('product_brand.index')->with('data' , $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = array(
            'page_name' => 'Product New Brand',
        );
        return view('product_brand.create')->with('data' , $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();
        request()->validate([
            'name' => 'required|max:20|min:3|regex:/^[A-Za-z_-]/',   
            'short_description' => 'required|max:100|min:3',   
            'meta_tag' => 'required|max:70|min:3',   
            'meta_description' => 'required|max:200|min:3'
        ]);

        
        if(isset($input['image'])){            
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('image/ProductBrand', $file_name, 'public');
            $input['image'] = $file_name;
        }else{
            $input['image'] = '';
        }

        unset($input['_token']);
        ProductBrand::create($input);        
        return redirect('product_brand')->with('flash_message' , "Product Brand Added");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = array(
            'page_name' => 'Product Brand',
            'arr' => ProductBrand::find($id),
        );
        

        return view('product_brand.show')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $category = ProductBrand::find($id);
        $input = $request->all();
        
        if(isset($input['image'])){            
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('image/ProductBrand', $file_name, 'public');
            $input['image'] = $file_name;
            Storage::disk('public')->delete('image/ProductBrand/' . $input['old_img']);
        }

        

        request()->validate([
            'name' => 'required|max:20|min:3|regex:/^[A-Za-z_-]/',            
        ]);

        unset($input['password']);

        $category->update($input);
        return redirect('product_brand')->with('flash_message', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $product_brand = ProductBrand::find($id);
        Storage::disk('public')->delete('image/ProductBrand' . $product_brand['image']);
        ProductBrand::destroy($id);
        return redirect('product_brand')->with('del_message', 'Deleted!');
    }
}
