<?php

namespace App\Http\Controllers;

use App\Models\ColorsModel;
use Illuminate\Http\Request;

class ColorsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = array(
            'page_name' => 'Colors',
            'arr' => ColorsModel::all(),
        );
        return view('colors.index')->with('data' , $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = array(
            'page_name' => 'Add Colors',
        );
        return view('colors.create')->with('data' , $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();
        request()->validate([
            'name' => 'required|max:20|min:1|regex:/^[A-Za-z_-]/',            
            'code' => 'required|max:20|min:1',
        ]);
        

        unset($input['_token']);
        ColorsModel::create($input);        
        return redirect('colors')->with('flash_message' , "Unit Added");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = array(
            'page_name' => 'Colors',
            'arr' => ColorsModel::find($id),
        );

        return view('colors.show')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $category = ColorsModel::find($id);
        $input = $request->all();
        

        request()->validate([
            'name' => 'required|max:20|min:1|regex:/^[A-Za-z_-]/',            
            'code' => 'required|max:20|min:1',           
        ]);

        $category->update($input);
        return redirect('colors')->with('flash_message', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        ColorsModel::destroy($id);
        return redirect('colors')->with('del_message', 'Deleted!');        
    }
}
