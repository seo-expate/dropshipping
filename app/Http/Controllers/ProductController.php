<?php

namespace App\Http\Controllers;

use App\Models\ProductModel;
use App\Models\TagsModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {        

        $arr = ProductModel::with('category', 'sub_category', 'product_brand', 'unit')->get();
        $page_name = 'Product';        
        return view('product.index',compact('arr','page_name'));
        
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        
        $page_name = 'Add New Product';
        $cat_arr = DB::table('categories')->where('status','=',1)->get();
        //$sub_cat_arr = DB::table('sub_categories')->where('status','=',1)->get();
        $brand_arr = DB::table('product_brands')->where('status','=',1)->get();
        $unit_arr = DB::table('units')->where('status','=',1)->get();
        $color_arr = DB::table('colors')->where('status','=',1)->get();        
        return view('product.create',compact('color_arr','cat_arr', 'brand_arr', 'unit_arr', 'page_name'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $input = $request->all();        

        request()->validate([
            'name' => 'required|max:20|min:3', 
            'sku' => 'required|max:100|unique:products|min:3',
            'colors' => 'required',
            'code' => 'required|max:100|min:3',
            'short_description' => 'required|max:100|min:3',
            'long_description' => 'required|max:100|min:3',
            'regular_price' => 'required|max:20|min:1',
            'selling_price' => 'required|max:20|min:1',
            'stock_amount' => 'required|max:20|min:1',
            'tags' => 'required',
            'image' => 'required',
        ]);        

        $tags = $input['tags'];
        
        if(isset($input['image'])){            
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('image/Product', $file_name, 'public');
            $input['image'] = "storage/image/Product/".$file_name;
        }else{
            $input['image'] = '';
        }

        unset($input['colors'], $input['tags'], $input['_token'], $input['otherImage'], $input['stock_amount']);

        $product_details = ProductModel::create($input);
        $tags_arr = array(
            'product_id' => 1,
            'tag' => 'test'
        );

        if($product_details){

            foreach($tags as $tag){
                // $tags_arr = array(
                //     'product_id' => $product_details['id'],
                //     'tag'    => $tag                    
                // );
                TagsModel::create($tags_arr);
            }
            
            
            //echo "<pre>" print_r($tags_arr[0]['tag'][2]);echo "</pre>";die;            
            
        }else{
            echo "HTTP";
        }

        return redirect('product')->with('flash_message' , "Product Added");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        
        $page_name = 'Product Brand';
        $arr = ProductModel::find($id);
        $tags_arr = json_decode(DB::table('tags')->where('product_id','=',$id)->get('tag'), true);        
       
        return view('product.show',compact('arr','page_name', 'tags_arr'));
        // return view('product.show',[
        //     'arr'=> ProductModel::find($id),
        //     'page_name'=> $page_name,
        // ]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $SubCategory = ProductModel::find($id);
        Storage::disk('public')->delete($SubCategory['image']);
        ProductModel::destroy($id);
        return redirect('product')->with('del_message', 'Deleted!');
    }
}
