<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $data = array(
            'page_name' => 'Category',
            'arr' => Category::all(),
        );
        return view('category.index')->with('data' , $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $data = array(
            'page_name' => 'Create New Category',
        );
        return view('category.create')->with('data' , $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        
        $input = $request->all();
        request()->validate([
            'name' => 'required|max:20|min:3|regex:/^[A-Za-z_-]/',   
        ]);

        
        if(isset($input['image'])){            
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('image/category', $file_name, 'public');
            $input['image'] = $file_name;
        }else{
            $input['image'] = '';
        }

        unset($input['_token']);
        Category::create($input);        
        return redirect('category')->with('flash_message' , "Category Added");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = array(
            'page_name' => 'Sub Category',
            'arr' => Category::find($id),
        );
        

        return view('category.show')->with('data', $data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $category = Category::find($id);
        $input = $request->all();
        
        if(isset($input['image'])){            
            $file_name = time().$request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs('image/category', $file_name, 'public');
            $input['image'] = $file_name;
            Storage::disk('public')->delete('image/category/' . $input['old_img']);
        }

        

        request()->validate([
            'name' => 'required|max:20|min:3|regex:/^[A-Za-z_-]/',
        ]);

        unset($input['password']);

        $category->update($input);
        return redirect('category')->with('flash_message', 'Updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $category = Category::find($id);
        DB::update('update sub_category set category_id = 0 where category_id = :id', ['id' => $id]);
        Storage::disk('public')->delete('image/category/' . $category['image']);
        Category::destroy($id);
        return redirect('category')->with('del_message', 'Deleted!');
    }

    public function uncategorized()
    {        
        $data = array(
            'page_name' => 'Uncategorized',
            'arr' => DB::select('select * from sub_categories where category_id = :id', ['id' => 0]),
        );
        return view('category.uncategorized')->with('data' , $data);
    }
}
