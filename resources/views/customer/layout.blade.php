<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Dashboard - NiceAdmin Bootstrap Template</title>
    <meta content="" name="description">
    <meta content="" name="keywords">

    <!-- Favicons -->
    <link href="assets/img/favicon.png" rel="icon">
    <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.gstatic.com" rel="preconnect">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="{{asset('assets/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/bootstrap-icons/bootstrap-icons.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/boxicons/css/boxicons.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/quill/quill.snow.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/quill/quill.bubble.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/remixicon/remixicon.css')}}" rel="stylesheet">
    <link href="{{asset('assets/vendor/simple-datatables/style.css')}}" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <!-- Template Main CSS File -->
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet">

    <!-- =======================================================
  * Template Name: NiceAdmin
  * Updated: Nov 17 2023 with Bootstrap v5.3.2
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

    <!-- ======= Header ======= -->
    @livewire('navigation-menu')
    <!-- ======= Sidebar ======= -->
    <aside id="sidebar" class="sidebar">

        <ul class="sidebar-nav" id="sidebar-nav">

            <li class="nav-item">
                <a class="nav-link " href="/dashboard">
                    <i class="bi bi-grid"></i>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" data-bs-target="#cutomer-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-menu-button-wide"></i><span>Customer</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="cutomer-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li>
                        <a href="{{route('customer.index')}}">
                            <i class="bi bi-circle"></i><span>List Customer</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{route('customer.create')}}">
                            <i class="bi bi-circle"></i><span>Add Customer</span>
                        </a>
                    </li>
                    
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" data-bs-target="#category-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-menu-button-wide"></i><span>Category</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="category-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li>
                        <a href="{{route('category.index')}}">
                            <i class="bi bi-circle"></i><span>Categories List</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{route('category.create')}}">
                            <i class="bi bi-circle"></i><span>Add Category</span>
                        </a>
                    </li>
                    
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" data-bs-target="#subCategory-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-menu-button-wide"></i><span>Sub Category</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="subCategory-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li>
                        <a href="{{route('sub_category.index')}}">
                            <i class="bi bi-circle"></i><span>Sub Categories List</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{route('sub_category.create')}}">
                            <i class="bi bi-circle"></i><span>Add Sub Category</span>
                        </a>
                    </li>
                    
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" href="/uncategorized">
                    <i class="bi bi-menu-button-wide"></i><span>Uncategorized</span>
                </a>                
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" data-bs-target="#productBrand-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-menu-button-wide"></i><span>Product Brand</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="productBrand-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li>
                        <a href="{{route('product_brand.index')}}">
                            <i class="bi bi-circle"></i><span>List</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{route('product_brand.create')}}">
                            <i class="bi bi-circle"></i><span>Add</span>
                        </a>
                    </li>
                    
                </ul>
            </li>


            <li class="nav-item">
                <a class="nav-link collapsed" data-bs-target="#unit-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-menu-button-wide"></i><span>Unit</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="unit-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li>
                        <a href="{{route('unit.index')}}">
                            <i class="bi bi-circle"></i><span>List</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{route('unit.create')}}">
                            <i class="bi bi-circle"></i><span>Add</span>
                        </a>
                    </li>
                    
                </ul>
            </li>


            <li class="nav-item">
                <a class="nav-link collapsed" data-bs-target="#colors-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-menu-button-wide"></i><span>Colors</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="colors-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li>
                        <a href="{{route('colors.index')}}">
                            <i class="bi bi-circle"></i><span>List</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{route('colors.create')}}">
                            <i class="bi bi-circle"></i><span>Add</span>
                        </a>
                    </li>
                    
                </ul>
            </li> 
            
            <li class="nav-item">
                <a class="nav-link collapsed" data-bs-target="#size-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-menu-button-wide"></i><span>Size</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="size-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li>
                        <a href="{{route('size.index')}}">
                            <i class="bi bi-circle"></i><span>List</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{route('size.create')}}">
                            <i class="bi bi-circle"></i><span>Add</span>
                        </a>
                    </li>
                    
                </ul>
            </li>

            <li class="nav-item">
                <a class="nav-link collapsed" data-bs-target="#product-nav" data-bs-toggle="collapse" href="#">
                    <i class="bi bi-menu-button-wide"></i><span>Product</span><i class="bi bi-chevron-down ms-auto"></i>
                </a>
                <ul id="product-nav" class="nav-content collapse " data-bs-parent="#sidebar-nav">
                    <li>
                        <a href="{{route('product.index')}}">
                            <i class="bi bi-circle"></i><span>List Products</span>
                        </a>
                    </li>

                    <li>
                        <a href="{{route('product.create')}}">
                            <i class="bi bi-circle"></i><span>Add Products</span>
                        </a>
                    </li>  
                    
                </ul>
            </li>
            
            

        </ul>

    </aside><!-- End Sidebar-->

    @yield('content')

    <!-- ======= Footer ======= -->
    <footer id="footer" class="footer">
        <div class="copyright">
            &copy; Copyright <strong><span>NiceAdmin</span></strong>. All Rights Reserved
        </div>
        <div class="credits">
            <!-- All the links in the footer should remain intact. -->
            <!-- You can delete the links only if you purchased the pro version. -->
            <!-- Licensing information: https://bootstrapmade.com/license/ -->
            <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
            Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
        </div>
    </footer><!-- End Footer -->

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

    <script src="{{asset('assets/vendor/apexcharts/apexcharts.min.js')}}"></script>
    <script src="{{asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
    <script src="{{asset('assets/vendor/chart.js/chart.umd.js')}}"></script>
    <script src="{{asset('assets/vendor/echarts/echarts.min.js')}}"></script>
    <script src="{{asset('assets/vendor/quill/quill.min.js')}}"></script>
    <script src="{{asset('assets/vendor/simple-datatables/simple-datatables.js')}}"></script>
    <script src="{{asset('assets/vendor/tinymce/tinymce.min.js')}}"></script>
    <script src="{{asset('assets/vendor/php-email-form/validate.js')}}"></script>

    <script src="{{asset('assets/js/main.js')}}"></script>

</body>

</html>