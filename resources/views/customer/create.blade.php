@extends('customer.layout')
@section('content')
<main id="main" class="main">
    <div class="pagetitle">
        <h1><?= $data['page_name']; ?></h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active"><?= $data['page_name']; ?></li>
            </ol>
        </nav>
    </div><!-- End Page Title -->
    
    <section class="section dashboard">
        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Add New</h5>
                        
                        <form class="row g-3" action="{{ route('customer.store') }}" method="post" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Your Name</label>
                                <input type="text" value="{{ old('name') }}" name="name" class="form-control" id="inputNanme4">
                                <b><small class="text-danger">{{ $errors->first('name') }}</small></b>
                            </div>
                            <div class="col-12">
                                <label for="inputEmail4" class="form-label">Email</label>
                                <input type="text" value="{{ old('email') }}" name="email" class="form-control" id="inputEmail4">
                                <b><small class="text-danger">{{ $errors->first('email') }}</small></b>
                            </div>
                            <div class="col-12">
                                <label for="phone" class="form-label">Phone</label>
                                <input type="text" value="{{ old('phone') }}" name="phone" class="form-control" id="phone">
                                <b><small class="text-danger">{{ $errors->first('phone') }}</small></b>
                            </div>
                            <div class="col-12">
                                <label for="inputPassword4" class="form-label">Password</label>
                                <input type="password" value="{{ old('password') }}" name="password" class="form-control" id="inputPassword4">
                                <b><small class="text-danger">{{ $errors->first('password') }}</small></b>
                            </div>
                            <div class="col-12">
                                <label for="inputAddress" class="form-label">Address</label>
                                <input type="text" value="{{ old('address') }}" name="address" class="form-control" id="inputAddress" placeholder="1234 Main St">
                                <b><small class="text-danger">{{ $errors->first('address') }}</small></b>
                            </div>
                            <div class="col-12">
                                <label for="image" class="form-label">Photo</label>
                                <input type="file" name="image" class="form-control" id="inputAddress" placeholder="1234 Main St">
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn btn-secondary">Reset</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </section>
</main>
@endsection