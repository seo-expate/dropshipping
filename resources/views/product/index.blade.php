@extends('customer.layout')
@section('content')

<main id="main" class="main">

    <div class="pagetitle">
        @if(Session::has('flash_message'))
        <br><br>
        <div class="alert alert-success">
            <h5>{{ Session::get('flash_message') }}</h5>
        </div>
        @endif

        @if(Session::has('del_message'))
        <br><br>
        <div class="alert alert-danger">
            <h5>{{ Session::get('del_message') }}</h5>
        </div>
        @endif
        <h1><?= $page_name; ?></h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active"><?= $page_name; ?></li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">All</h5>

                        <!-- Default Table -->
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Name</th>                                    
                                        <th scope="col">Category</th>                         
                                        <th scope="col">Sub Category</th>                         
                                        <th scope="col">Brand</th>                         
                                        <th scope="col">Unit</th>                         
                                        <th scope="col">SKU</th>                         
                                        <th scope="col">Code</th>                         
                                        <th scope="col">Short Description</th>                         
                                        <th scope="col">Long Description</th>                         
                                        <th scope="col">Image</th>                         
                                        <th scope="col">Regular Price</th>                         
                                        <th scope="col">Selling Price</th>                         
                                        <th scope="col">Stock Amount</th>                         
                                        <th scope="col">Hit Count</th>                         
                                        <th scope="col">Sale Count</th>                         
                                        <th scope="col">Featured Status</th>
                                        <th scope="col">Refund</th>
                                        <th scope="col">Tags</th>
                                        <th scope="col">Shipping Cost</th>
                                        <th scope="col">Cash On</th>
                                        <th scope="col">Flash Deal</th>
                                        <th scope="col">Flash Deal Discount</th>
                                        <th scope="col">Status</th>
                                        <th scope="col">Action</th>
                                    </tr>
                                </thead>

                                <tbody>
                                    @foreach($arr as $item)
                                    <tr>
                                        <th scope="row">{{ $loop->iteration }}</th>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->category->name }}</td>
                                        <td>{{ $item->sub_category->name }}</td>
                                        <td>{{ $item->product_brand->name }}</td>
                                        <td>{{ $item->unit->code }}</td>
                                        <td>{{ $item->sku }}</td>
                                        <td>{{ $item->code }}</td>
                                        <td>{{ $item->short_description }}</td>
                                        <td>{{ $item->long_description }}</td>
                                        <td>{{ $item->image }}</td>
                                        <td>{{ $item->regular_price }}</td>
                                        <td>{{ $item->selling_price }}</td>
                                        <td>{{ $item->stock_amount }}</td>
                                        <td>{{ $item->hit_count }}</td>
                                        <td>{{ $item->sales_count }}</td>
                                        <td>{{ $item->featured_status }}</td>
                                        <td>{{ $item->refund }}</td>
                                        <td>{{ $item->tags }}</td>
                                        <td>{{ $item->shipping_cost }}</td>
                                        <td>{{ $item->cash_on }}</td>
                                        <td>{{ $item->flash_deal }}</td>
                                        <td>{{ $item->flash_deal_discount }}</td>
                                        <td>{{ $item->status == 1 ? 'Active' :'Inactive'}}
                                            
                                        </td>
                                        <td>
                                            <a href="{{ url('/product/'. $item->id)}}"><button class="btn btn-sm btn-success">View</button></a>
                                            <form method="POST" action="{{ url('/product' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete Student" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                        <!-- End Default Table Example -->
                    </div>
                </div>
            </div>

        </div>
    </section>
</main>
@endsection