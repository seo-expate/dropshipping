@extends('customer.layout')
@section('content')

<style>
  .other_images{max-width: 160px; height: auto;}

</style>
<main id="main" class="main">

  <div class="pagetitle">
    <h1>Profile</h1>
    <nav>
      <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
        <li class="breadcrumb-item">Product</li>
        <li class="breadcrumb-item active">View</li>
      </ol>
    </nav>
  </div><!-- End Page Title -->

  <section class="section profile">
    <div class="row">

      <div class="col-xl-12">

        <div class="card">
          <div class="card-body pt-3">
            <!-- Bordered Tabs -->
            <ul class="nav nav-tabs nav-tabs-bordered">

              <li class="nav-item">
                <button class="nav-link active" data-bs-toggle="tab" data-bs-target="#profile-overview">Overview</button>
              </li>

              <li class="nav-item">
                <button class="nav-link" data-bs-toggle="tab" data-bs-target="#profile-edit">Edit</button>
              </li>

            </ul>
            <div class="tab-content pt-2">

              <div class="tab-pane fade show active profile-overview" id="profile-overview">

                <h5 class="card-title">Details</h5>
                
                <div class="row">
                  <div class="col-lg-9 col-md-8"><img src="{{ asset($arr->image) }}" alt="Profile" class="img-fluid"></div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Name</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->name }}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Category</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->category->name }}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Sub Category</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->sub_category->name }}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Brand</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->product_brand->name }}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Unit</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->unit->code }}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">SKU</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->sku }}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Code</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->code }}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Short Description</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->short_description }}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Long Description</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->long_description }}</div>
                </div>

                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Other Images</div>
                  <div class="col-lg-9 col-md-8">
                    <img class="other_images" src="{{ asset($arr->image) }}" alt="Profile" class="img-fluid">
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Regular Price</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->regular_price }}</div>
                </div>
                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Selling Price</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->selling_price }}</div>
                </div>
                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Stock Amount</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->stock_amount }}</div>
                </div>
                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Hit Count</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->hit_count }}</div>
                </div>
                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Sale Count</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->sales_count }}</div>
                </div>
                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Featured Status</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->featured_status }}</div>
                </div>
                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Refund</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->refund }}</div>
                </div>
                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Tags</div>
                  <div class="col-lg-9 col-md-8">
                      
                  <!-- tags_arr -->
                  @foreach($tags_arr as $tag)
                    {{ json_decode($tag->tag) }}
                  @endforeach
                      
                  </div>
                </div>
                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Shipping Cost</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->shipping_cost }}</div>
                </div>
                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Cash On</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->cash_on }}</div>
                </div>
                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Flash Deal</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->flash_deal }}</div>
                </div>
                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Flash Deal Discount</div>
                  <div class="col-lg-9 col-md-8">{{ $arr->flash_deal_discount }}</div>
                </div>
                <div class="row">
                  <div class="col-lg-3 col-md-4 label ">Status</div>
                  <div class="col-lg-9 col-md-8">{{ ($arr->status == 1 )? 'Active' : 'Inactive' }}</div>
                </div>
              </div>

              <div class="tab-pane fade profile-edit pt-3" id="profile-edit">

                <!-- Profile Edit Form -->
                <form action="{{ url('product/'.$arr->id) }}" method="POST" enctype="multipart/form-data">
                  <div class="row mb-3">
                    <label for="profileImage" class="col-md-4 col-lg-3 col-form-label">Change Profile Image</label>
                    <div class="col-md-8 col-lg-9">

                      <img src="{{ asset('/storage/image/ProductBrand/'.$arr->image) }}" alt="Profile" class="img-fluid">

                      <div class="pt-2">
                        <input name="image" type="file" class="form-control" id="fullName">
                      </div>
                    </div>
                  </div>

                  {!! csrf_field() !!}
                  {{ method_field('PUT') }}

                  <input type="hidden" name='old_img' value="{{ $arr->image }}">

                  <div class="row mb-3">
                    <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Name</label>
                    <div class="col-md-8 col-lg-9">
                      <input name="name" type="text" class="form-control" id="fullName" value="{{ $arr->name }}">
                      <b><small class="text-danger">{{ $errors->first('name') }}</small></b>
                    </div>
                  </div>

                  <div class="row mb-3">
                    <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Short Description</label>
                    <div class="col-md-8 col-lg-9">
                      <input name="short_description" type="text" class="form-control" id="fullName" value="{{ $arr->short_description }}">
                      <b><small class="text-danger">{{ $errors->first('short_description') }}</small></b>
                    </div>
                  </div>

                  <div class="row mb-3">
                    <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Meta Tag</label>
                    <div class="col-md-8 col-lg-9">
                      <input name="meta_tag" type="text" class="form-control" id="fullName" value="{{ $arr->meta_tag }}">
                      <b><small class="text-danger">{{ $errors->first('meta_tag') }}</small></b>
                    </div>
                  </div>

                  <div class="row mb-3">
                    <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Meta Description</label>
                    <div class="col-md-8 col-lg-9">
                      <input name="meta_description" type="text" class="form-control" id="fullName" value="{{ $arr->meta_description }}">
                      <b><small class="text-danger">{{ $errors->first('meta_description') }}</small></b>
                    </div>
                  </div>


                  <div class="text-center">
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                  </div>
                </form><!-- End Profile Edit Form -->

              </div>

              <div class="tab-pane fade pt-3" id="profile-settings">

                <!-- Settings Form -->
                <form>

                  <div class="row mb-3">
                    <label for="fullName" class="col-md-4 col-lg-3 col-form-label">Email Notifications</label>
                    <div class="col-md-8 col-lg-9">
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="changesMade" checked>
                        <label class="form-check-label" for="changesMade">
                          Changes made to your account
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="newProducts" checked>
                        <label class="form-check-label" for="newProducts">
                          Information on new products and services
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="proOffers">
                        <label class="form-check-label" for="proOffers">
                          Marketing and promo offers
                        </label>
                      </div>
                      <div class="form-check">
                        <input class="form-check-input" type="checkbox" id="securityNotify" checked disabled>
                        <label class="form-check-label" for="securityNotify">
                          Security alerts
                        </label>
                      </div>
                    </div>
                  </div>

                  <div class="text-center">
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                  </div>
                </form><!-- End settings Form -->

              </div>

            </div><!-- End Bordered Tabs -->

          </div>
        </div>

      </div>
    </div>
  </section>
</main>
@endsection