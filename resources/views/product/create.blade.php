@extends('customer.layout')
@section('content')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css" />

<style>
    .color_name {
        width: 90px;
    }

    .color_code {
        width: 40px;
        height: 20px;
    }

    /* upload image css */
    .upload {
        &__box {
            padding: 40px;
        }

        &__inputfile {
            width: .1px;
            height: .1px;
            opacity: 0;
            overflow: hidden;
            position: absolute;
            z-index: -1;
        }

        &__btn {
            display: inline-block;
            font-weight: 600;
            color: #fff;
            text-align: center;
            min-width: 116px;
            padding: 5px;
            transition: all .3s ease;
            cursor: pointer;
            border: 2px solid;
            background-color: #4045ba;
            border-color: #4045ba;
            border-radius: 10px;
            line-height: 26px;
            font-size: 14px;

            &:hover {
                background-color: unset;
                color: #4045ba;
                transition: all .3s ease;
            }

            &-box {
                margin-bottom: 10px;
            }
        }

        &__img {
            &-wrap {
                display: flex;
                flex-wrap: wrap;
                margin: 0 -10px;
            }

            &-box {
                width: 200px;
                padding: 0 10px;
                margin-bottom: 12px;
            }

            &-close {
                width: 24px;
                height: 24px;
                border-radius: 50%;
                background-color: rgba(0, 0, 0, 0.5);
                position: absolute;
                top: 10px;
                right: 10px;
                text-align: center;
                line-height: 24px;
                z-index: 1;
                cursor: pointer;

                &:after {
                    content: '\2716';
                    font-size: 14px;
                    color: white;
                }
            }
        }
    }

    .img-bg {
        background-repeat: no-repeat;
        background-position: center;
        background-size: cover;
        position: relative;
        padding-bottom: 100%;
    }

    .upload__img-box {
        width: 171px;
        height: auto;
        float: left;
        margin-right: 10px;
        margin-top: 10px;
    }

    .upload__img-close {
        width: 24px;
        height: 24px;
        border-radius: 50%;
        background-color: rgba(0, 0, 0, 0.5);
        position: absolute;
        top: 10px;
        right: 10px;
        text-align: center;
        line-height: 24px;
        z-index: 1;
        cursor: pointer;
    }

    .upload__img-close:after {
        content: "✖";
        font-size: 14px;
        color: white;
    }
</style>
<main id="main" class="main">
    <div class="pagetitle">
        <h1><?= $page_name; ?></h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active"><?= $page_name; ?></li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Add New</h5>

                        <form class="row g-3" action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
                            {!! csrf_field() !!}
                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Name</label>
                                <input type="text" value="{{ old('name') }}" name="name" class="form-control" id="inputNanme4">
                                <b><small class="text-danger">{{ $errors->first('name') }}</small></b>
                            </div>

                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Upload Product Banner</label>
                                <input type="file" value="{{ old('image') }}" name="image" class="form-control" id="inputNanme4">
                                <b><small class="text-danger">{{ $errors->first('image') }}</small></b>
                            </div>

                            <div class="col-12">                                
                                <div class="upload__box">
                                    <div class="upload__btn-box">
                                        <label class="upload__btn">
                                            <p>Other Upload images</p>
                                            <input name="otherImage[]" type="file" multiple="" data-max_length="20" class="upload__inputfile">
                                            <p><b><small class="text-danger">{{ $errors->first('otherImage') }}</small></b></p>                                            
                                        </label>
                                    </div>
                                    <div class="upload__img-wrap"></div>
                                </div>
                            </div>

                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Color</label>
                                @foreach ($color_arr as $item)
                                <div class="form-check">
                                    <input class="form-check-input" name="colors[]" type="checkbox" value="{{$item->code}}" id="flexCheckDefault">
                                    <label class="form-check-label" for="flexCheckDefault">
                                        <div>
                                            <span class="color_name float-start">{{$item->name}}</span> <span class="float-start color_code" style="background: {{$item->code}}"></span>
                                        </div>
                                    </label>
                                </div>
                                @endforeach
                                <b><small class="text-danger">{{ $errors->first('colors') }}</small></b>
                                </select>
                            </div>

                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Product Code</label>
                                <input type="text" value="{{ old('code') }}" name="code" class="form-control" id="inputNanme4">
                                <b><small class="text-danger">{{ $errors->first('code') }}</small></b>
                            </div>

                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Category</label>
                                <select name="category_id" class="form-select" aria-label="Default select example">
                                    <option value="">Select</option>
                                    @foreach ($cat_arr as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Sub Category</label>
                                <select name="sub_category_id" class="form-select" aria-label="Default select example">
                                    <option value="">Select</option>                                    
                                </select>
                            </div>

                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Product Brand</label>
                                <select name="brand_id" class="form-select" aria-label="Default select example">
                                    <option value="">Select</option>
                                    @foreach ($brand_arr as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Unit</label>
                                <select name="unit_id" class="form-select" aria-label="Default select example">
                                    <option value="">Select</option>
                                    @foreach ($unit_arr as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Stock Amount</label>
                                <input type="text" value="{{ old('stock_amount') }}" name="stock_amount" class="form-control" id="inputNanme4">
                                <b><small class="text-danger">{{ $errors->first('stock_amount') }}</small></b>
                            </div>

                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">SKU</label>
                                <input type="text" value="{{ old('sku') }}" name="sku" class="form-control" id="inputNanme4">
                                <b><small class="text-danger">{{ $errors->first('sku') }}</small></b>
                            </div>                            

                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Short Description</label>
                                <input type="text" value="{{ old('short_description') }}" name="short_description" class="form-control" id="inputNanme4">
                                <b><small class="text-danger">{{ $errors->first('short_description') }}</small></b>
                            </div>

                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Long Description</label>
                                <textarea type="text" value="{{ old('long_description') }}" name="long_description" class="form-control" id="inputNanme4"></textarea>
                                <b><small class="text-danger">{{ $errors->first('long_description') }}</small></b>
                            </div>

                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Regular Price</label>
                                <input type="text" value="{{ old('regular_price') }}" name="regular_price" class="form-control" id="inputNanme4">
                                <b><small class="text-danger">{{ $errors->first('regular_price') }}</small></b>
                            </div>

                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Selling Price</label>
                                <input type="text" value="{{ old('selling_price') }}" name="selling_price" class="form-control" id="inputNanme4">
                                <b><small class="text-danger">{{ $errors->first('selling_price') }}</small></b>
                            </div>                            

                            <div class="col-12">
                                <label for="inputNanme4" class="form-label">Tags</label>
                                <select class="form-control select2" name="tags[]" multiple="multiple"></select>
                                <b><small class="text-danger">{{ $errors->first('tags') }}</small></b>
                            </div>
                            
                            <div class="text-center">
                                <button type="submit" class="btn btn-primary">Submit</button>
                                <button type="reset" class="btn btn-secondary">Reset</button>
                            </div>
                        </form>

                    </div>
                </div>
            </div>

        </div>
    </section>
</main>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js" integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/js/select2.full.min.js"></script>
<script>
    $('.select2').select2({
        //data: ["rfrews", "donald"],
        tags: true,
        maximumSelectionLength: 10,
        tokenSeparators: [',', ' '],
        placeholder: "Select or type keywords",
    });

    $(document).ready(function() {
        ImgUpload();
    });

    function ImgUpload() {
        var imgWrap = "";
        var imgArray = [];

        $('.upload__inputfile').each(function() {
            $(this).on('change', function(e) {
                imgWrap = $(this).closest('.upload__box').find('.upload__img-wrap');
                var maxLength = $(this).attr('data-max_length');

                var files = e.target.files;
                var filesArr = Array.prototype.slice.call(files);
                var iterator = 0;
                filesArr.forEach(function(f, index) {

                    if (!f.type.match('image.*')) {
                        return;
                    }

                    if (imgArray.length > maxLength) {
                        return false
                    } else {
                        var len = 0;
                        for (var i = 0; i < imgArray.length; i++) {
                            if (imgArray[i] !== undefined) {
                                len++;
                            }
                        }
                        if (len > maxLength) {
                            return false;
                        } else {
                            imgArray.push(f);

                            var reader = new FileReader();
                            reader.onload = function(e) {
                                var html = "<div class='upload__img-box'><div style='background-image: url(" + e.target.result + ")' data-number='" + $(".upload__img-close").length + "' data-file='" + f.name + "' class='img-bg'><div class='upload__img-close'></div></div></div>";
                                imgWrap.append(html);
                                iterator++;
                            }
                            reader.readAsDataURL(f);
                        }
                    }
                });
            });
        });

        $('body').on('click', ".upload__img-close", function(e) {
            var file = $(this).parent().data("file");
            for (var i = 0; i < imgArray.length; i++) {
                if (imgArray[i].name === file) {
                    imgArray.splice(i, 1);
                    break;
                }
            }
            $(this).parent().parent().remove();
        });
    }

    $(document).ready(function(){
        
        $('select[name="category_id"]').on("change", function(){
            let category_id = $('select[name="category_id"]').val();

            $.ajax({
                type: 'GET',
                url: '/get_sub_category',
                data : {
                    'category_id' : category_id
                },
                dataType: 'html',
                success: function (data) {
                    $('select[name="sub_category_id"]').html(data)
                },error:function(){ 
                    //console.log(data);
                }
            });            
        });
        
    });
</script>
@endsection