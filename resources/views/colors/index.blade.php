@extends('customer.layout')
@section('content')
<main id="main" class="main">

    <div class="pagetitle">
        @if(Session::has('flash_message'))
        <br><br>
        <div class="alert alert-success">
            <h5>{{ Session::get('flash_message') }}</h5>
        </div>
        @endif

        @if(Session::has('del_message'))
        <br><br>
        <div class="alert alert-danger">
            <h5>{{ Session::get('del_message') }}</h5>
        </div>
        @endif
        <h1><?= $data['page_name']; ?></h1>
        <nav>
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                <li class="breadcrumb-item active"><?= $data['page_name']; ?></li>
            </ol>
        </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">All</h5>

                        <!-- Default Table -->
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>                                    
                                    <th scope="col">Code</th>                                    
                                    <th scope="col">Status</th>                                   
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach($data['arr'] as $item)
                                <tr>
                                    <th scope="row">{{ $loop->iteration }}</th>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->code }}</td>
                                    <td>{{ $item->status }}</td>
                                    <td>
                                        <a href="{{ url('/colors/'. $item->id)}}"><button class="btn btn-sm btn-success">View</button></a>
                                        <form method="POST" action="{{ url('/colors' . '/' . $item->id) }}" accept-charset="UTF-8" style="display:inline">
                                            {{ method_field('DELETE') }}
                                            {{ csrf_field() }}
                                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Student" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach

                            </tbody>
                        </table>
                        <!-- End Default Table Example -->
                    </div>
                </div>
            </div>

        </div>
    </section>
</main>
@endsection