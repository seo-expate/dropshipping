<?php

use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ColorsController;
use App\Http\Controllers\SubCategoryController;
use App\Http\Controllers\ProducBrandController;
use App\Http\Controllers\SizeController;
use App\Http\Controllers\UnitController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware([
    'auth:sanctum',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');
    
    Route::resource('customer', CustomerController::class);
    Route::get('uncategorized', [CategoryController::class, 'uncategorized']);
    Route::resource('category', CategoryController::class);
    Route::resource('sub_category', SubCategoryController::class);
    Route::resource('product_brand', ProducBrandController::class);
    Route::resource('unit', UnitController::class);
    Route::resource('colors', ColorsController::class);
    Route::resource('size', SizeController::class);
    Route::resource('product', ProductController::class);
    Route::get('get_sub_category', [SubCategoryController::class, 'get_sub_category']);
});
